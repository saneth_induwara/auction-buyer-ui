import React from "react";
import AuctionRoomTile from "./AuctionRoomTile";
import axios from 'util/Api'

class Dashboard extends React.Component {

    _isMounted = false;

    data = [
        {
            "roomId":1,
            "auctions":[
                {
                    "name":"CSI Private(LTD)",
                    "logo":"https://i.pinimg.com/originals/7e/b2/ae/7eb2ae7c40fcadf5cf04950a97929d05.png",
                    "sellingMark":340000,
                    "minBid":340000,
                    "highestBid":340000,
                    "myBid":340000,
                    "auctionStatus":"live"
                },
                {
                    "name":"CSI Private(LTD) 2",
                    "logo":"https://cdn.dribbble.com/users/11373/screenshots/493150/cardinal.png",
                    "sellingMark":340000,
                    "minBid":340000,
                    "highestBid":340000,
                    "myBid":340000,
                    "auctionStatus":"over"
                },
                {
                    "name":"CSI Private(LTD)",
                    "logo":"https://i.pinimg.com/originals/7e/b2/ae/7eb2ae7c40fcadf5cf04950a97929d05.png",
                    "sellingMark":340000,
                    "minBid":340000,
                    "highestBid":340000,
                    "myBid":340000,
                    "auctionStatus":"live"
                },
                {
                    "name":"CSI Private(LTD) 2",
                    "logo":"https://cdn.dribbble.com/users/11373/screenshots/493150/cardinal.png",
                    "sellingMark":340000,
                    "minBid":340000,
                    "highestBid":340000,
                    "myBid":340000,
                    "auctionStatus":"over"
                },
                {
                    "name":"CSI Private(LTD)",
                    "logo":"https://i.pinimg.com/originals/7e/b2/ae/7eb2ae7c40fcadf5cf04950a97929d05.png",
                    "sellingMark":340000,
                    "minBid":340000,
                    "highestBid":340000,
                    "myBid":340000,
                    "auctionStatus":"live"
                }
            ]
        },
        {
            "roomId":2,
            "auctions":[
                {
                    "name":"CSI Private(LTD)",
                    "logo":"",
                    "sellingMark":340000,
                    "minBid":340000,
                    "highestBid":340000,
                    "myBid":0,
                    "auctionStatus":"next"
                },
                {
                    "name":"CSI Private(LTD) 2`",
                    "logo":"",
                    "sellingMark":340000,
                    "minBid":340000,
                    "highestBid":340000,
                    "myBid":0,
                    "auctionStatus":"next"
                }
            ]
        },
        {
            "roomId":3,
            "auctions":[
                {
                    "name":"CSI Private(LTD)",
                    "logo":"",
                    "sellingMark":340000,
                    "minBid":340000,
                    "highestBid":340000,
                    "myBid":340000,
                    "auctionStatus":"next"
                }
            ]
        },
        {
            "roomId":4,
            "auctions":[
                {
                    "name":"CSI Private(LTD)",
                    "logo":"",
                    "sellingMark":340000,
                    "minBid":340000,
                    "highestBid":340000,
                    "myBid":340000,
                    "auctionStatus":"over"
                }
            ]
        },
        {
            "roomId":5,
            "auctions":[
                {
                    "name":"CSI Private(LTD)",
                    "logo":"",
                    "sellingMark":340000,
                    "minBid":340000,
                    "highestBid":340000,
                    "myBid":340000,
                    "auctionStatus":"over"
                }
            ]
        },
        {
            "roomId":6,
            "auctions":[
                {
                    "name":"CSI Private(LTD)",
                    "logo":"",
                    "sellingMark":340000,
                    "minBid":340000,
                    "highestBid":340000,
                    "myBid":340000,
                    "auctionStatus":"over"
                }
            ]
        },
        {
            "roomId":7,
            "auctions":[
                {
                    "name":"CSI Private(LTD)",
                    "logo":"",
                    "sellingMark":340000,
                    "minBid":340000,
                    "highestBid":340000,
                    "myBid":340000,
                    "auctionStatus":"over"
                }
            ]
        }
    ];

    constructor(props) {
        super(props);
        this.state = {auctionRooms: []}
    }

    getAuctionRooms() {
        // axios.get('auctionRooms')
        //     .then(({data}) => {
        //         if (this._isMounted) {
        //             this.setState({ auctionRooms: data });
        //         }
        //     }).catch(function (error) {
        //     console.log("Error****:", error.message);
        // });
        if (this._isMounted) {
            this.setState({ auctionRooms: this.data });
        }
    }

    componentDidMount() {
        this._isMounted = true;
        this.getAuctionRooms();

    }

    componentWillUnmount() {
        this._isMounted = false;

    }

    render() {
        return (
            <div className="mx-5 mt-5 h-100">
                <div className="h-100">
                    <div className="row">
                        {this.state.auctionRooms.map((data, index) =>
                            <div key={index} className="col-sm-6 col-md-4 col-lg-3 col-12">
                                <AuctionRoomTile data={data}/>
                            </div>
                        )}
                    </div>

                </div>

            </div>
        );
    }
}

export default Dashboard;
