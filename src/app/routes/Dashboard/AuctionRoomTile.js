import React from "react";
import Widget from "components/Widget/index";
import AuctionItem from "./AuctionItem";
import Slider from "react-slick";


class AuctionRoomTile extends React.Component{


    constructor(props) {
        super(props);

        this.state = {
            slider: null,
            showArrows: false
        };

    }

    componentDidMount() {
        this.setState({slider: this.refs.slider, showArrows: this.props.data.auctions.length>1})
    }


    render() {
        const settings = {
            arrows: false,
            dots: true,
            infinite: true,
            lazyLoad: true,
            speed: 500,
            marginLeft: 10,
            marginRight: 10,
            slidesToShow: 1,
            slidesToScroll: 1,
            appendDots: dots => (
                <div className="mb-0"
                    style={{
                        marginBottom: "0",
                        borderRadius: "10px",
                        bottom: "15px"
                        // padding: "100px 10px"
                    }}
                >
                    <ul className="d-flex justify-content-center px-0" style={{ margin: "0px" }}> {dots} </ul>
                </div>
            ),
        };

        return (
            <Widget styleName="jr-card-full">
                <Slider ref="slider" className="jr-slick-slider" {...settings}>
                    {this.props.data.auctions.map((media, index) =>
                        <AuctionItem showArrows={this.state.showArrows} key={index} data={media} slider={this.state.slider}
                        />
                    )
                    }
                </Slider>
            </Widget>
        );
    }
};

export default AuctionRoomTile;
