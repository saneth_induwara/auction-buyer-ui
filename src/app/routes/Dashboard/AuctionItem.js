import React from "react";
import {Badge, Button} from "reactstrap";
import Avatar from "@material-ui/core/Avatar";
import Divider from "@material-ui/core/Divider";
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

class AuctionItem extends React.Component {

    state = {
        open: false,
    };


    handleClickOpen = () => {
        this.setState({open: true});
    };

    handleRequestClose = () => {
        this.setState({open: false});
    };

    prevClick() {
        this.props.slider.slickPrev();
    }

    nextClick() {
        this.props.slider.slickNext();
    }


    render() {
        return (
            <div className={`jr-slider ${this.props.showArrows?'pb-3':''}`}>
                <div className={`d-flex flex-row px-3 pt-3`}>
                    <Badge className={
                        `ml-auto mt-1 text-white font-size-12
                    ${this.props.data.auctionStatus === 'live' ? "bg-green" : ""}
                    ${this.props.data.auctionStatus === 'next' ? "bg-blue" : ""}
                    ${this.props.data.auctionStatus === 'over' ? "bg-red" : ""}`
                    } color="">{this.props.data.auctionStatus}</Badge>
                </div>
                <div className="text-center px-3 pt-0">
                    <div className="d-flex flex-row align-items-center justify-content-center mb-3">
                        {this.props.showArrows ?
                            <Button className="bg-transparent round-button mr-2" color=""
                                    onClick={() => this.prevClick()}>
                                <i className="zmdi zmdi-chevron-left jr-fs-xxl text-grey"/>
                            </Button>
                            : ''}

                        <Avatar className="size-60" alt="..." src={this.props.data.logo}/>
                        {this.props.showArrows ?
                            <Button className="bg-transparent round-button ml-2" color=""
                                    onClick={() => this.nextClick()}>
                                <i className="zmdi zmdi-chevron-right jr-fs-xxl text-grey"/>
                            </Button>
                            : ''}
                    </div>
                    <div>
                        <h4 className="mb-1 font-weight-semibold">{this.props.data.name}</h4>
                        <div className="d-flex flex-row justify-content-center">
                            <p className="text-grey">Lot No 1233</p>
                            <p className="text-grey ml-2">Grade highest</p>
                        </div>
                    </div>
                </div>
                <div className="d-flex flex-row mx-4 pb-1">
                    <i className="zmdi zmdi-money mr-1 my-auto"/>
                    <span className="flex-grow-1">Selling Mark</span>
                    <span className="ml-3 font-weight-semibold">Rs {this.props.data.sellingMark}</span>
                </div>
                <div className="d-flex flex-row mx-4 pb-1">
                    <i className="zmdi zmdi-long-arrow-down mr-1 my-auto"/>
                    <span className="flex-grow-1">Min Bid</span>
                    <span className="ml-3 font-weight-semibold">Rs {this.props.data.minBid}</span>
                </div>
                <div className="d-flex flex-row mx-4">
                    <i className="zmdi zmdi-long-arrow-up mr-1 my-auto"/>
                    <span className="flex-grow-1">Highest Bid</span>
                    <span className="ml-3 font-weight-semibold">Rs {this.props.data.highestBid}</span>
                </div>
                <Divider className="mt-3" light/>
                <div className="d-flex flex-row mx-4 mt-3 mb-3">
                    <span className="flex-grow-1 font-weight-bold my-auto">My Bid</span>
                    {this.props.data.myBid === 0 ?
                        <Button variant="raised" color="primary" className="jr-btn text-white"
                                onClick={this.handleClickOpen}>Place Bid</Button> :
                        <Button variant="raised" color="primary"
                                className="jr-btn bg-transparent text-primary"
                                onClick={this.handleClickOpen}>Rs {this.props.data.myBid}</Button>
                    }
                    <Dialog open={this.state.open} onClose={this.handleRequestClose}>
                        <DialogTitle className="pb-0">{this.props.data.name}</DialogTitle>
                        <DialogContent>
                            <div className="d-flex flex-row">
                                <p className="text-grey">Lot No 1233</p>
                                <p className="text-grey ml-2">Grade highest</p>
                            </div>
                            <div className="d-flex flex-row pb-1">
                                <i className="zmdi zmdi-money mr-1 my-auto"/>
                                <span className="flex-grow-1">Selling Mark</span>
                                <span className="ml-3 font-weight-semibold">Rs {this.props.data.sellingMark}</span>
                            </div>
                            <div className="d-flex flex-row pb-1">
                                <i className="zmdi zmdi-long-arrow-down mr-1 my-auto"/>
                                <span className="flex-grow-1">Min Bid</span>
                                <span className="ml-3 font-weight-semibold">Rs {this.props.data.minBid}</span>
                            </div>
                            <div className="d-flex flex-row">
                                <i className="zmdi zmdi-long-arrow-up mr-1 my-auto"/>
                                <span className="flex-grow-1">Highest Bid</span>
                                <span className="ml-3 font-weight-semibold">Rs {this.props.data.highestBid}</span>
                            </div>
                            <div className="d-flex flex-row mt-2 mb-2">
                                <div>
                                    <TextField
                                        autoFocus
                                        margin="dense"
                                        id="myBid"
                                        label="Bid Amount"
                                        type="number"
                                        fullWidth
                                    />
                                </div>
                                <div className="mt-auto ml-2">
                                    <Button onClick={this.handleRequestClose} color="secondary">
                                        Place Bid
                                    </Button>
                                </div>

                            </div>

                        </DialogContent>
                    </Dialog>
                </div>
            </div>
        );
    }

};

export default AuctionItem;
