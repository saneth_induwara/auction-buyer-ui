import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom'
import {useDispatch, useSelector} from 'react-redux';
import Button from '@material-ui/core/Button';
import IntlMessages from 'util/IntlMessages';
import {userSignIn} from 'actions/Auth';

const SignIn = (props) => {

  const [email, setEmail] = useState('demo@example.com');
  const [password, setPassword] = useState('demo#123');
  const dispatch = useDispatch();
  const token = useSelector(({auth}) => auth.token);
  const {loading, message} = useSelector(({commonData}) => commonData);

  useEffect(() => {
    if (token !== null) {
      props.history.push('/');
    }
  }, [token]);

  return (
      <div
          className="app-login-container d-flex justify-content-center align-items-center animated slideInUpTiny animation-duration-3">
        <div className="login-content">
          <div className="login-header mt-2 d-flex justify-content-center">
            <Link className="app-logo" to="/" title="Logo">
              <img src={require("assets/images/logo-color.png")} alt="logo" title="logo"/>
            </Link>
          </div>

          <div className="login-form mt-4">
            <form>
              <fieldset>
                <div className="form-group">
                  <input name="email" id="email" className="form-control form-control-lg"
                         placeholder="Email" type="email"
                         onChange={(event) => setEmail(event.target.value)}/>
                </div>
                <div className="form-group">
                  <input name="password" id="password" className="form-control form-control-lg"
                         placeholder="Password" type="password"
                         onChange={(event) => setPassword(event.target.value)}/>
                </div>
                <div className="form-group mt-4 d-flex justify-content-between align-items-center">
                  <label className="custom-control custom-checkbox float-left pl-0">
                    <input type="checkbox" className="custom-control-input"/>
                    <span className="custom-control-indicator"/>
                    <span className="custom-control-description">Remember me</span>
                  </label>

                  <div>
                    <Link to="/app/app-module/forgot-password-1"
                          title="Reset Password"><IntlMessages id="appModule.forgotPassword"/></Link>
                  </div>
                </div>

                <Button className="bg-primary py-2 text-white mt-3" fullWidth={true}
                        onClick={() => {dispatch(userSignIn({email, password}));}}>
                  <IntlMessages id="appModule.signIn"/>
                </Button>
                <div className="d-flex justify-content-center mt-3">
                  <Link to="/signup">
                    <IntlMessages id="signIn.signUp"/>
                  </Link>
                </div>

              </fieldset>
            </form>
          </div>
        </div>
      </div>
  );
};


export default SignIn;
