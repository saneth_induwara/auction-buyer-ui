import {FETCH_ERROR, FETCH_START, FETCH_SUCCESS} from "../constants/ActionTypes";
import axios from 'util/Api'

export const getAuctionRooms = () => {
    return (dispatch) => {
        dispatch({type: FETCH_START});
        axios.get('auctionRooms')
            .then(({data}) => {
                dispatch({type: FETCH_SUCCESS});
        }).catch(function (error) {
            dispatch({type: FETCH_ERROR, payload: error.message});
            console.log("Error****:", error.message);
        });
    }
};
